package rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Application {
    // todo: Zhdankin: стоит поработать над слоями в приложении: должен быть слой контроллеров, сервисов и доступа к данным,
    // todo: Zhdankin: а сущности и dto так же должны соблюдать некий контекст слоя.
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
