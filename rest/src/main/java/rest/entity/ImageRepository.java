package rest.entity;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<AlbumImage, Long>{

	Optional<AlbumImage> findById(UUID aImageId);

}
