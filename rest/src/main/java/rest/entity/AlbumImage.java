package rest.entity;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.http.MediaType;

/**
 * Класс описывающий изображение, содержащиеся в альбоме. Хранимая сущность.
 * 
 * @see Album
 **/
@Entity
public class AlbumImage {
	/** Идентификатор изображения, содержащегося в альбоме. **/
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
		)
	// todo: Zhdankin: по такому id сейчас можно предсказать кол-во изображений и скачать любое изображение, давай попробуем както использовать UUID?
	private UUID id;
	/** Имя файла, содержащего изображение. **/
	private String fileName;
	/** Содержимое, т.е. само изображение. **/
	@Lob
	private byte[] content;
	@ManyToOne
	private Album album;
	/** формат изображения png/jpg/gif **/
	private MediaType format;

	public AlbumImage(UUID id, String fileName, byte[] content, Album album, String format) {
		this.id = id;
		this.fileName = fileName;
		this.content = content;
		this.album = album;
		switch (format) {
		case "image/jpeg":
			this.format = MediaType.IMAGE_JPEG;
			break;
		case "image/png":
			this.format = MediaType.IMAGE_PNG;
			break;
		case "image/gif":
			this.format = MediaType.IMAGE_GIF;
			break;
		default:
			this.format = MediaType.APPLICATION_OCTET_STREAM;
			break;
		}
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public MediaType getFormat() {
		return format;
	}

	public void setFormat(MediaType format) {
		this.format = format;
	}

	protected AlbumImage() {
	}
}
