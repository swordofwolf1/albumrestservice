package rest.entity;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.List;
import java.util.UUID;

/** Альбом с изображениями. Хранимая сущность. **/
@Entity
public class Album {
	/** Идентификатор альбома **/
	@Id
	@GeneratedValue
	private Long id;
	/** Название альбома **/
	private String albumName;
	/** Изображения, содержащиеся в альбоме. **/

	/* todo: Zhdankin: точно ли нужно использование ElementCollection для Entity? */
	/* не знаю  */
	@ElementCollection
	@OneToMany(cascade = CascadeType.ALL,  orphanRemoval = true)
	private List<AlbumImage> images;

	protected Album() {}
	
	public Album(long id, String albumName, List<AlbumImage> images) {
		this.id = id;
		this.albumName = albumName;
		this.images = images;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public List<AlbumImage> getImages() {
		return images;
	}

	protected void setImages(List<AlbumImage> images) {
		this.images = images;
	}

}
