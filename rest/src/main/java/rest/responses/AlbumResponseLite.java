package rest.responses;

import java.util.List;

/** Альбом, облегчённая версия(без изображений), ответ для запросов: /albums, /albums/create **/
public class AlbumResponseLite {
	/** Идентификатор альбома. **/
	private long id;
	/** Название альбома. **/
	private String name;

	// todo: Zhdankin: зачем в объете поле ошибки?
	// Tsiryulnikov: эксперементировал с вариантами ответов на запросы, нужно было как-то возвращать ошибку, в качестве временной заплатки, возвращал её через поле в классе ответа


	public AlbumResponseLite(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
