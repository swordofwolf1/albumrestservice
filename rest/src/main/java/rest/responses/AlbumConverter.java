package rest.responses;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import rest.entity.Album;

@Component
public class AlbumConverter {

	private AlbumImageConverter imageConverter = new AlbumImageConverter();

	/** Конвертирует Album в AlbumResponseLite. **/
	public AlbumResponseLite convert(Album album) {
		return new AlbumResponseLite(album.getId(), album.getAlbumName());
	}

	/** Конвертирует список Album в список AlbumResponseLite **/
	public List<AlbumResponseLite> convert(List<Album> albums) {
		ArrayList<AlbumResponseLite> retValue = new ArrayList<>(albums.size());
		for (Album item : albums)
			retValue.add(convert(item));
		return retValue;
	}

	public AlbumResponse convertToAlbumResponse(Album album) {
		return new AlbumResponse(album.getId(), album.getAlbumName(), imageConverter.convert(album.getImages()));
	}
}
