package rest.responses;

import java.util.UUID;

/**
 * Информация о изображении из альбома. Легковесный вариант, содержит только
 * название и идентификтаор изображения.
 * 
 * @see AlbumResponse Составная часть ответа на запрос /album{id}.
 **/
public class AlbumImageResponse {

	/** Идентификатор изображения **/
	private UUID id;
	/** Название изображения **/
	private String name;

	
	public AlbumImageResponse(UUID id, String name) {
		this.id = id;
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
