package rest.responses;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import rest.entity.AlbumImage;

@Component
public class AlbumImageConverter {

	/** Конвертирует AlbumImage в AlbumImageResponse **/
	public AlbumImageResponse convert(AlbumImage albumImage) {
		return new AlbumImageResponse(albumImage.getId(), albumImage.getFileName());
	}

	/** Конвертирует список AlbumImage в список AlbumImageResponse **/
	public List<AlbumImageResponse> convert(List<AlbumImage> albumImages) {
		ArrayList<AlbumImageResponse> retValue = new ArrayList<>(albumImages.size());
		for (AlbumImage item : albumImages)
			retValue.add(convert(item));
		return retValue;
	}
}
