package rest.responses;

import java.util.UUID;

/** Ответ на запрос album/{id}/addImage **/
public class AddImageResponse {
	/** Идентификатор добавленного в альбом изображения **/
	private UUID id;
	/** название добавленного в альом изображения **/
	private String name;
	/** размер добавленного в альбом изображения **/
	private long size;
	/** формат **/
	private String format;

	public AddImageResponse(UUID id, String name, long size, String format) {
		this.id = id;
		this.name = name;
		this.size = size;
		this.format = format;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

}
