package rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import javassist.NotFoundException;
import rest.entity.Album;
import rest.entity.AlbumImage;
import rest.entity.AlbumRepository;
import rest.entity.ImageRepository;
import rest.responses.AddImageResponse;
import rest.responses.AlbumConverter;
import rest.responses.AlbumImageConverter;
import rest.responses.AlbumResponse;
import rest.responses.AlbumResponseLite;
import rest.services.AlbumService;
import rest.services.exceptions.CreateAlbumException;
import rest.services.exceptions.EmptyFileException;
import rest.services.exceptions.FileFormatException;

@RestController
@RequestMapping("/api")
public class AlbumController {

	private AlbumService albumService;

	/* todo: Zhdankin: лучше сделать инжект через конструктор */
	/* todo: Zhdankin: конвертеры тоже нужно инжектить */
	/* Вот так? */
	@Autowired
	public AlbumController(AlbumService aAlbumService) {
		albumService = aAlbumService;
	}

	@GetMapping("/albums")
	// todo: Zhdankin: по код-стайлу джавы, следует писать методы со строчной буквы,
	// но в рамках ТЗ не критично
	public ResponseEntity<List<AlbumResponseLite>> getAlbums() {
		/*
		 * todo: Zhdankin: нужен слой сервисов между контролером и репозиторием - туда
		 * вынести вызов конвертера. Так для других методов контроллера
		 */
		return new ResponseEntity<>(albumService.getAlbums(), HttpStatus.OK);
	}

	@PostMapping("/albums/create")
	/*
	 * todo: Zhdankin: сделать передачу имени альбома не параметром, а в теле json
	 */
	/*
	 * Tsiryulnikov: понял почему название в body, для безопасности. Если строка в
	 * body передаётся, она экранируется
	 */
	public ResponseEntity<AlbumResponseLite> createAlbum(@RequestBody String albumName) throws CreateAlbumException {
		AlbumResponseLite retValue = null;
		retValue = albumService.createAlbum(albumName);
		return new ResponseEntity<>(retValue, HttpStatus.CREATED);

	}

	@DeleteMapping("/albums/delete/{id}")
	public ResponseEntity<List<AlbumResponseLite>> deleteAlbum(@PathVariable long id) throws NotFoundException {
		// todo: Zhdankin: нужно более корректное решение для возврата разного типа
		// объектов
		/* todo: Zhdankin: зачем нужно чтобы delete-метод возвращал список альбомов? */
		/*
		 * Tsiryulnikov: Это было удобно для отладки, таким образом после удаления я
		 * получал в ответ список оставшихся альбомов
		 */
		return new ResponseEntity<List<AlbumResponseLite>>(albumService.deleteAlbum(id), HttpStatus.OK);
	}

	@PostMapping("album/{id}/addImage")
	public ResponseEntity<AddImageResponse> addImage(@RequestParam MultipartFile file, @PathVariable long id)
			throws NotFoundException, FileFormatException, EmptyFileException, IOException {
		return new ResponseEntity<AddImageResponse>(albumService.addImage(file, id), HttpStatus.OK);
	}

	@GetMapping("album/{id}")
	public ResponseEntity<AlbumResponse> getAlbum(@PathVariable long id) throws NotFoundException {
		return new ResponseEntity<AlbumResponse>(albumService.getAlbum(id), HttpStatus.OK);
	}

	@GetMapping("album/download/{imageId}")
	// todo: Zhdankin: зачем нужна переменная albumId?
	// не нужна
	public HttpEntity<byte[]> downloadImage(@PathVariable UUID imageId) throws NotFoundException {

		AlbumImage findedImage = albumService.downdloadImage(imageId);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(findedImage.getFormat());
		headers.setContentLength(findedImage.getContent().length);
		/*
		 * todo: Zhdankin: нужно как то возвращать оригинальное файл, то, что было,
		 * когда файл загрузили
		 */
		/*
		 * Tsiryulnikov: не совсем понимаю, что под этим подразумевается ?
		 */
		return new HttpEntity<byte[]>(findedImage.getContent(), headers);
	}

	@DeleteMapping("album/{albumId}/deleteImage/{imageId}")
	public ResponseEntity<AlbumResponse> deleteImage(@PathVariable long albumId, @PathVariable UUID imageId) throws NotFoundException {
		return new ResponseEntity<AlbumResponse>(
				albumService.deleteImage(albumId, imageId), HttpStatus.OK);
	}

	@PutMapping("album/{id}/rename")
	public ResponseEntity<AlbumResponse> renameAlbum(@RequestBody String newName,
			@PathVariable long id) throws NotFoundException {
		return new ResponseEntity<AlbumResponse>(albumService.renameAlbum(id, newName), HttpStatus.OK);
	}
}
