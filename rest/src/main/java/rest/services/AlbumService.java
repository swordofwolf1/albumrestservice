package rest.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import javassist.NotFoundException;
import rest.AlbumController;
import rest.entity.Album;
import rest.entity.AlbumImage;
import rest.entity.AlbumRepository;
import rest.entity.ImageRepository;
import rest.responses.AddImageResponse;
import rest.responses.AlbumConverter;
import rest.responses.AlbumImageConverter;
import rest.responses.AlbumResponse;
import rest.responses.AlbumResponseLite;
import rest.services.exceptions.CreateAlbumException;
import rest.services.exceptions.EmptyFileException;
import rest.services.exceptions.FileFormatException;

/** Сервис для работы с албомами и изображениями, используется в @see {@link AlbumController} **/
@Component
public class AlbumService {

	/* Репозиторий альбомов. */
	private AlbumRepository albumRepo;
	/* Репозиторий изображений. */
	private ImageRepository imageRepo;
	/* Конвертер Album в AlbumResponse/AlbumResponseLite. */
	private AlbumConverter albumConverter;
	/* Конвертер изображений из альбома в респонсы. */
	private AlbumImageConverter imageConverter;

	@Autowired
	public AlbumService(AlbumRepository aAlbumRepo, ImageRepository aImageRepo, AlbumConverter aAlbumConverter,
			AlbumImageConverter aImageConverter) {

		this.albumRepo = aAlbumRepo;
		this.imageRepo = aImageRepo;
		this.albumConverter = aAlbumConverter;
		this.imageConverter = aImageConverter;
	}

	
	public List<AlbumResponseLite> getAlbums() {
		List<AlbumResponseLite> retValue = albumConverter.convert(Lists.newArrayList(albumRepo.findAll()));
		return retValue;
	}

	public AlbumResponseLite createAlbum(String aName) throws CreateAlbumException {
		Album createdAlbum;
		try {
			createdAlbum = albumRepo.save(new Album(-1, aName, new ArrayList<AlbumImage>()));
		} catch (Exception e) {
			throw new CreateAlbumException("Не удалось создать: " + e.getStackTrace());
		}
		if (createdAlbum == null) {
			throw new CreateAlbumException("Не удалось создать");
		}
		return albumConverter.convert(createdAlbum);
	}

	public List<AlbumResponseLite> deleteAlbum(long aAlbumId) throws NotFoundException {
		Optional<Album> findedAlbum = albumRepo.findById(aAlbumId);
		if (!findedAlbum.isPresent()) {
			throw new NotFoundException("Альбом с идентификатором: " + aAlbumId + "не найден");
		}
		albumRepo.deleteById(aAlbumId);
		return albumConverter.convert(Lists.newArrayList(albumRepo.findAll()));
	}

	public AddImageResponse addImage(MultipartFile aFile, long aAlbumId)
			throws NotFoundException, FileFormatException, EmptyFileException, IOException {
		AddImageResponse retValue = null;
		if (aFile.isEmpty()) {
			throw new EmptyFileException("Файл не может быть пустым");
		}

		if (!aFile.getContentType().contains("image")) {
			throw new FileFormatException("Неверный формат файла, допускаются только JPEG, GIF, PNG");
		}

		Optional<Album> findedAlbum = albumRepo.findById(aAlbumId);
		if (!findedAlbum.isPresent()) {
			throw new NotFoundException("альбом с идентификатором: " + aAlbumId + " не найден");
		}

		Album album = findedAlbum.get();
		addImage(new AlbumImage(new UUID(0, 0), aFile.getName(), aFile.getBytes(), album, aFile.getContentType()), album.getImages());
		album = albumRepo.save(album);
		retValue = new AddImageResponse(album.getImages().get(album.getImages().size() - 1).getId(), aFile.getName(),
				aFile.getSize(), aFile.getContentType());
		return retValue;
	}

	public AlbumResponse getAlbum(long aAlbumId) throws NotFoundException {
		AlbumResponse retValue = null;
		Optional<Album> findedAlbum = albumRepo.findById(aAlbumId);

		if (!findedAlbum.isPresent()) {
			throw new NotFoundException("альбом с идентификатором: " + aAlbumId + " не найден");
		}

		retValue = albumConverter.convertToAlbumResponse(findedAlbum.get());
		return retValue;
	}

	public AlbumImage downdloadImage(UUID aImageId) throws NotFoundException {
		Optional<AlbumImage> findedImage = imageRepo.findById(aImageId);
		if (!findedImage.isPresent()) {
			throw new NotFoundException("Изображание с идентификатором: " + aImageId + " не найдено");
		}
		return findedImage.get();
	}

	public AlbumResponse deleteImage(long aAlbumId, UUID aImageId) throws NotFoundException {
		AlbumResponse retValue = null;
		Optional<Album> findedAlbum = albumRepo.findById(aAlbumId);
		if (!findedAlbum.isPresent()) {
			throw new NotFoundException("Альбом с id: " + aAlbumId + "не найден");
		}

		Optional<AlbumImage> findedImage = imageRepo.findById(aImageId);
		if (!findedImage.isPresent()) {
			throw new NotFoundException("Изображение с id: " + aImageId + "не найден");
		}
		Album album = findedAlbum.get();

		if (!isContainsImage(findedImage.get(), album)) {
			throw new NotFoundException("Изображение с id:" + aImageId + " не содержится в альбоме с id:" + aAlbumId);
		}
		deleteImageById(aImageId, album.getImages());
		imageRepo.delete(findedImage.get());
		retValue = albumConverter.convertToAlbumResponse(albumRepo.findById(aAlbumId).get());
		return retValue;
	}

	public AlbumResponse renameAlbum(long aAlbumId, String aNewName) throws NotFoundException {
		Optional<Album> finded = albumRepo.findById(aAlbumId);
		if (!finded.isPresent()) {
			throw new NotFoundException("Альбом с id: " + aAlbumId + "не найден");
		}
		Album updating = finded.get();
		updating.setAlbumName(aNewName);
		albumRepo.save(updating);
		Album updated = albumRepo.findById(aAlbumId).get();
		return albumConverter.convertToAlbumResponse(updated);
	}
	
	
	private void addImage(AlbumImage aImage, List<AlbumImage> aImages) {
		/* todo: Zhdankin: эту логику лучше вынести в сервисный слой */
		aImages.add(aImage);
	}
	
	private void deleteImageById(UUID aId, List<AlbumImage> aImages) {
		/* todo: Zhdankin: эту логику лучше вынести в сервисный слой */
		AlbumImage image = null;
		for(AlbumImage img : aImages ) {
			if(img.getId().equals(aId)) {
				image = img;
			}
		}
		if(image ==null) {
			System.out.println("image is null");
			System.out.println("argument UUID:" + aId);
		}
		if(image != null) {
			aImages.remove(image);
		}
	}
	
	private boolean isContainsImage(AlbumImage aImage, Album aAlbum ) {
		System.out.println("aImageId:" + aImage.getId());
		for(int i = 0; i< aAlbum.getImages().size(); i++) {
			if(aAlbum.getImages().get(i).getId().equals(aImage.getId())) {
				System.out.println("is contains = true");
				return true;
			}
			System.out.println("List element [" + i + "] imageId: " +  aAlbum.getImages().get(i).getId());
		}
		/* todo: Zhdankin: эту логику лучше вынести в сервисный слой */
		return false;
		
	}
}
