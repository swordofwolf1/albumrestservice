package rest.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EmptyFileException extends Exception {

	private static final long serialVersionUID = 807051039189324404L;

	public EmptyFileException(String aMessage) {
		super(aMessage);
	}
}
