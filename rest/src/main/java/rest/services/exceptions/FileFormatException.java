package rest.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
public class FileFormatException extends Exception {

	private static final long serialVersionUID = 6560112477898356877L;

	public FileFormatException(String aMessage) {
		super(aMessage);
	}
}
