package rest.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class CreateAlbumException extends Exception {
	private static final long serialVersionUID = -1452995492950443055L;
	
	public CreateAlbumException(String aMessage) {
		super(aMessage);
	}
}
