# AlbumRestService
Простенький rest сервис. 
Умеет: 
- создавать, изменять, удалять, получать альбомы
- добавлять изображения в альбомы, скачивать изображения из альбомов

Инструкция по сборке приложения
0) Импортируем проект в Eclipse IDE 
[-] Чтобы импортировать проект: File -> Import -> General -> Exicting Project In Workspace
1) Создаём конфигурацию Mavenbuild, в ней указывем Goals: clean install
[-] Чтобы создать конфигурацию: Run -> RunConfigurations -> Клик ПКМ на MavenBuild -> New Maven Configuration
[](https://drive.google.com/file/d/1if1Xth1vhSrGOXVUoAZwYYLCmcfP8As4/view?usp=sharing)
2) Запускаем с помощью конфигурации которую создали (ждём пока скачаются все билиотеки)
3) Запускаем как JavaApplication
[](https://drive.google.com/file/d/168C9cD7I60ckUBg5skW-8UyoxUgyHNcg/view?usp=sharing)
4) Заходим на http://localhost:8080/swagger-ui.html
5) Раскрываем AlbumController и проверяем функциональность
